package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Trade;

@Repository
public class MySQLTradeRepository implements TradeRepository {

	@Autowired
	JdbcTemplate template;

	@Override
	public List<Trade> getAllTrades() {
		// TODO Auto-generated method stub
		String sql = "SELECT id, stockTicker, price, volume, buyOrSell, statusCode FROM Trade";
		return template.query(sql, new TradeRowMapper());
	}

	@Override
	public Trade getTradeById(int id) {
		// TODO Auto-generated method stub
		String sql = "SELECT id, stockTicker, price, volume, buyOrSell, statusCode FROM Trade WHERE id=?";
		return template.queryForObject(sql, new TradeRowMapper(), id);
	}

	@Override
	public Trade editTrade(Trade trade) {
		// TODO Auto-generated method stub
		String sql = "UPDATE Trade SET id = ?, stockTicker = ?, price = ?, volume = ?, buyOrSell = ?, statusCode = ?"
				+ "WHERE id = ?";
		template.update(sql, sql, trade.getId(), trade.getStockTicker(), trade.getPrice(), trade.getVolume(),
				trade.getBuyOrSell(), trade.getStatusCode());
		return trade;
	}

	@Override
	public int deleteTrade(int id) {
		// TODO Auto-generated method stub
		String sql = "DELETE FROM Trade WHERE id = ?";
		template.update(sql, id);
		return id;
	}

	@Override
	public Trade addTrade(Trade trade) {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO Trade(id, stockTicker, price, volume, buyOrSell, statusCode) "
				+ "VALUES(?,?,?,?,?,?)";
		System.out.println(sql);
		template.update(sql, trade.getId(), trade.getStockTicker(), trade.getPrice(), trade.getVolume(),
				trade.getBuyOrSell(), trade.getStatusCode());
		return trade;
	}

}

class TradeRowMapper implements RowMapper<Trade> {
	@Override
	public Trade mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Trade(rs.getInt("id"), rs.getString("stockTicker"), rs.getDouble("price"), rs.getInt("volume"),
				rs.getString("buyOrSell"), rs.getInt("statusCode"));
	}
}