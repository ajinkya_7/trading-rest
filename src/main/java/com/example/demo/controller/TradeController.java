package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Trade;
import com.example.demo.service.TradeService;

@RestController
@RequestMapping("/api/trade")
public class TradeController {

	@Autowired
	TradeService service;

	@GetMapping(value = "/get")
	public List<Trade> getAllTrades() {
		return service.getAllTrades();
	}

	@GetMapping(value = "/{id}")
	public Trade getTradeById(@PathVariable("id") int id) {
		return service.getTrade(id);
	}

	@PostMapping(value = "/")
	public Trade addTrade(@RequestBody Trade trade) {
		return service.newTrade(trade);
	}

	@PutMapping(value = "/")
	public Trade editTrade(@RequestBody Trade trade) {
		return service.saveTrade(trade);
	}

	@DeleteMapping(value = "/{id}")
	public int deleteOrder(@PathVariable int id) {
		return service.deleteTrade(id);
	}

}